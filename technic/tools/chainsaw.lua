-- Configuration

local chainsaw_max_charge_mini =  15000
local chainsaw_max_charge      =  30000 -- Maximum charge of the saw
local chainsaw_max_charge_mk2  = 120000

local chainsaw_charge_per_log = 15
local chainsaw_charge_per_leaf = 7

-- Box radius within which leaves should also be removed
local leaves_reach = 2

-- The default trees
local timber_nodenames = {
	["default:acacia_tree"] = true,
	["default:aspen_tree"]  = true,
	["default:jungletree"]  = true,
	["default:papyrus"]     = true,
	["default:cactus"]      = true,
	["default:tree"]        = true,
	["default:apple"]       = true,
	["default:pine_tree"]   = true,
}

local leaf_nodenames = {
	["default:acacia_leaves"] = true,
	["default:aspen_leaves"] = true,
	["default:leaves"] = true,
	["default:jungleleaves"] = true,
	["default:pine_needles"] = true
}

-- technic_worldgen defines rubber trees if moretrees isn't installed
if minetest.get_modpath("technic_worldgen") or
		minetest.get_modpath("moretrees") then
	timber_nodenames["moretrees:rubber_tree_trunk_empty"] = true
	timber_nodenames["moretrees:rubber_tree_trunk"]       = true

	leaf_nodenames["moretrees:rubber_tree_leaves"] = true
end

-- Support moretrees if it is there
if minetest.get_modpath("moretrees") then
	timber_nodenames["moretrees:acacia_trunk"]                     = true
	timber_nodenames["moretrees:apple_tree_trunk"]                 = true
	timber_nodenames["moretrees:beech_trunk"]                      = true
	timber_nodenames["moretrees:birch_trunk"]                      = true
	timber_nodenames["moretrees:fir_trunk"]                        = true
	timber_nodenames["moretrees:oak_trunk"]                        = true
	timber_nodenames["moretrees:palm_trunk"]                       = true
	timber_nodenames["moretrees:pine_trunk"]                       = true
	timber_nodenames["moretrees:sequoia_trunk"]                    = true
	timber_nodenames["moretrees:spruce_trunk"]                     = true
	timber_nodenames["moretrees:willow_trunk"]                     = true
	timber_nodenames["moretrees:jungletree_trunk"]                 = true
	timber_nodenames["moretrees:poplar_trunk"]                     = true
	timber_nodenames["moretrees:cedar_trunk"]                      = true
	timber_nodenames["moretrees:date_palm_fruit_trunk"]            = true
	timber_nodenames["moretrees:rubber_tree_trunk"]                = true
	timber_nodenames["moretrees:rubber_tree_trunk_empty"]          = true

	leaf_nodenames["moretrees:acacia_leaves"]            = true
	leaf_nodenames["moretrees:apple_tree_leaves"]        = true
	leaf_nodenames["moretrees:oak_leaves"]               = true
	leaf_nodenames["moretrees:cedar_leaves"]             = true
	leaf_nodenames["moretrees:cedar_cone"]               = true
	leaf_nodenames["moretrees:fir_leaves"]               = true
	leaf_nodenames["moretrees:fir_leaves_bright"]        = true
	leaf_nodenames["moretrees:sequoia_leaves"]           = true
	leaf_nodenames["moretrees:birch_leaves"]             = true
	leaf_nodenames["moretrees:palm_leaves"]              = true
	leaf_nodenames["moretrees:spruce_leaves"]            = true
	leaf_nodenames["moretrees:pine_leaves"]              = true
	leaf_nodenames["moretrees:willow_leaves"]            = true
	leaf_nodenames["moretrees:jungletree_leaves_green"]  = true
	leaf_nodenames["moretrees:jungletree_leaves_yellow"] = true
	leaf_nodenames["moretrees:jungletree_leaves_red"]    = true
	leaf_nodenames["moretrees:acorn"]                    = true
	leaf_nodenames["moretrees:coconut"]                  = true
	leaf_nodenames["moretrees:coconut_1"]                = true
	leaf_nodenames["moretrees:coconut_2"]                = true
	leaf_nodenames["moretrees:coconut_3"]                = true
	leaf_nodenames["moretrees:spruce_cone"]              = true
	leaf_nodenames["moretrees:pine_cone"]                = true
	leaf_nodenames["moretrees:fir_cone"]                 = true
	leaf_nodenames["moretrees:apple_blossoms"]           = true
	leaf_nodenames["moretrees:poplar_leaves"]            = true
	leaf_nodenames["moretrees:dates_f0"]                 = true
	leaf_nodenames["moretrees:dates_f1"]                 = true
	leaf_nodenames["moretrees:dates_f2"]                 = true
	leaf_nodenames["moretrees:dates_f3"]                 = true
	leaf_nodenames["moretrees:dates_f4"]                 = true
	leaf_nodenames["moretrees:dates_fn"]                 = true
	leaf_nodenames["moretrees:dates_m0"]                 = true
	leaf_nodenames["moretrees:dates_n"]                  = true
end

-- Support growing_trees
if minetest.get_modpath("growing_trees") then
	timber_nodenames["growing_trees:trunk"]         = true
	timber_nodenames["growing_trees:medium_trunk"]  = true
	timber_nodenames["growing_trees:big_trunk"]     = true
	timber_nodenames["growing_trees:trunk_top"]     = true
	timber_nodenames["growing_trees:trunk_sprout"]  = true
	timber_nodenames["growing_trees:branch_sprout"] = true
	timber_nodenames["growing_trees:branch"]        = true
	timber_nodenames["growing_trees:branch_xmzm"]   = true
	timber_nodenames["growing_trees:branch_xpzm"]   = true
	timber_nodenames["growing_trees:branch_xmzp"]   = true
	timber_nodenames["growing_trees:branch_xpzp"]   = true
	timber_nodenames["growing_trees:branch_zz"]     = true
	timber_nodenames["growing_trees:branch_xx"]     = true

	leaf_nodenames["growing_trees:leaves"] = true
end

-- Support growing_cactus
if minetest.get_modpath("growing_cactus") then
	timber_nodenames["growing_cactus:sprout"]                       = true
	timber_nodenames["growing_cactus:branch_sprout_vertical"]       = true
	timber_nodenames["growing_cactus:branch_sprout_vertical_fixed"] = true
	timber_nodenames["growing_cactus:branch_sprout_xp"]             = true
	timber_nodenames["growing_cactus:branch_sprout_xm"]             = true
	timber_nodenames["growing_cactus:branch_sprout_zp"]             = true
	timber_nodenames["growing_cactus:branch_sprout_zm"]             = true
	timber_nodenames["growing_cactus:trunk"]                        = true
	timber_nodenames["growing_cactus:branch_trunk"]                 = true
	timber_nodenames["growing_cactus:branch"]                       = true
	timber_nodenames["growing_cactus:branch_xp"]                    = true
	timber_nodenames["growing_cactus:branch_xm"]                    = true
	timber_nodenames["growing_cactus:branch_zp"]                    = true
	timber_nodenames["growing_cactus:branch_zm"]                    = true
	timber_nodenames["growing_cactus:branch_zz"]                    = true
	timber_nodenames["growing_cactus:branch_xx"]                    = true
end

-- Support ethereal
if minetest.get_modpath("ethereal") then
	timber_nodenames["ethereal:willow_trunk"]          = true
	timber_nodenames["ethereal:redwood_trunk"]         = true
	timber_nodenames["ethereal:frost_tree"]            = true
	timber_nodenames["ethereal:yellow_trunk"]          = true
	timber_nodenames["ethereal:birch_trunk"]           = true
	timber_nodenames["ethereal:palm_trunk"]            = true
	timber_nodenames["ethereal:banana_trunk"]          = true
	timber_nodenames["ethereal:bamboo"]                = true
	timber_nodenames["ethereal:mushroom_trunk"]        = true
	timber_nodenames["ethereal:sakura_trunk"]	         = true
	timber_nodenames["ethereal:scorched_tree"]         = true
	-- some extras
	timber_nodenames["ethereal:olive_trunk"]         = true

	leaf_nodenames["ethereal:willow_twig"]        = true
	leaf_nodenames["ethereal:redwood_leaves"]     = true
	leaf_nodenames["ethereal:frost_leaves"]       = true
	leaf_nodenames["ethereal:yellowleaves"]       = true
	leaf_nodenames["ethereal:birch_leaves"]       = true
	leaf_nodenames["ethereal:palmleaves"]         = true
	leaf_nodenames["ethereal:bananaleaves"]       = true
	leaf_nodenames["ethereal:bamboo_leaves"]      = true
	leaf_nodenames["ethereal:mushroom"]           = true
	leaf_nodenames["ethereal:mushroom_pore"]      = true
	leaf_nodenames["ethereal:orange_leaves"]      = true
	leaf_nodenames["ethereal:sakura_leaves"]      = true
	leaf_nodenames["ethereal:sakura_leaves2"]     = true
	-- fruits
	leaf_nodenames["ethereal:banana"]        = true
	leaf_nodenames["ethereal:orange"]        = true
	leaf_nodenames["ethereal:coconut"]       = true
	leaf_nodenames["ethereal:golden_apple"]  = true
	-- extra
	leaf_nodenames["ethereal:vine"]  = true
	-- some extras
	leaf_nodenames["ethereal:olive_leaves"]  = true
	leaf_nodenames["ethereal:olive"]         = true
	leaf_nodenames["ethereal:lemon_leaves"]  = true
	leaf_nodenames["ethereal:lemon"]         = true
end

-- Support maple
if minetest.get_modpath("maple") then
	timber_nodenames["maple:maple_tree"]         = true
	leaf_nodenames["maple:maple_leaves"] = true
end

-- Support farming_plus
if minetest.get_modpath("farming_plus") then
	leaf_nodenames["farming_plus:banana_leaves"] = true
	leaf_nodenames["farming_plus:banana"]        = true
	leaf_nodenames["farming_plus:cocoa_leaves"]  = true
	leaf_nodenames["farming_plus:cocoa"]         = true
end

-- Support nature
if minetest.get_modpath("nature") then
	leaf_nodenames["nature:blossom"] = true
end

-- Support snow
if minetest.get_modpath("snow") then
	leaf_nodenames["snow:needles"] = true
	leaf_nodenames["snow:needles_decorated"] = true
	leaf_nodenames["snow:star"] = true
end

--Support australia (if present)
if minetest.get_modpath("australia") then
	timber_nodenames["australia:black_box_tree"] = true
	timber_nodenames["australia:black_wattle_tree"] = true
	timber_nodenames["australia:blue_gum_tree"] = true
	timber_nodenames["australia:boab_tree"] = true
	timber_nodenames["australia:bull_banksia_tree"] = true
	timber_nodenames["australia:celery_top_pine_tree"] = true
	timber_nodenames["australia:cherry_tree"] = true
	timber_nodenames["australia:cloncurry_box_tree"] = true
	timber_nodenames["australia:coast_banksia_tree"] = true
	timber_nodenames["australia:coolabah_tree"] = true
	timber_nodenames["australia:daintree_stringybark_tree"] = true
	timber_nodenames["australia:darwin_woollybutt_tree"] = true
	timber_nodenames["australia:desert_oak_tree"] = true
	timber_nodenames["australia:fan_palm_tree"] = true
	timber_nodenames["australia:golden_wattle_tree"] = true
	timber_nodenames["australia:grey_mangrove_tree"] = true
	timber_nodenames["australia:huon_pine_tree"] = true
	timber_nodenames["australia:illawarra_flame_tree"] = true
	timber_nodenames["australia:jarrah_tree"] = true
	timber_nodenames["australia:karri_tree"] = true
	timber_nodenames["australia:lemon_eucalyptus_tree"] = true
	timber_nodenames["australia:lemon_myrtle_tree"] = true
	timber_nodenames["australia:lilly_pilly_tree"] = true
	timber_nodenames["australia:macadamia_tree"] = true
	timber_nodenames["australia:mangrove_apple_tree"] = true
	timber_nodenames["australia:merbau_tree"] = true
	timber_nodenames["australia:marri_tree"] = true
	timber_nodenames["australia:moreton_bay_fig_tree"] = true
	timber_nodenames["australia:mulga_tree"] = true
	timber_nodenames["australia:paperbark_tree"] = true
	timber_nodenames["australia:quandong_tree"] = true
	timber_nodenames["australia:red_bottlebrush_tree"] = true
	timber_nodenames["australia:river_oak_tree"] = true
	timber_nodenames["australia:river_red_gum_tree"] = true
	timber_nodenames["australia:rottnest_island_pine_tree"] = true
	timber_nodenames["australia:scribbly_gum_tree"] = true
	timber_nodenames["australia:shoestring_acacia_tree"] = true
	timber_nodenames["australia:snow_gum_tree"] = true
	timber_nodenames["australia:southern_sassafras_tree"] = true
	timber_nodenames["australia:stilted_mangrove_tree"] = true
	timber_nodenames["australia:sugar_gum_tree"] = true
	timber_nodenames["australia:swamp_bloodwood_tree"] = true
	timber_nodenames["australia:swamp_gum_tree"] = true
	timber_nodenames["australia:swamp_paperbark_tree"] = true
	timber_nodenames["australia:tasmanian_myrtle_tree"] = true
	timber_nodenames["australia:tea_tree_tree"] = true
	timber_nodenames["australia:white_box_tree"] = true
	timber_nodenames["australia:wirewood_tree"] = true
	timber_nodenames["australia:fern_trunk"] = true
	timber_nodenames["australia:fern_trunk_big"] = true

	leaf_nodenames["australia:black_box_leaves"] = true
	leaf_nodenames["australia:black_wattle_leaves"] = true
	leaf_nodenames["australia:blue_gum_leaves"] = true
	leaf_nodenames["australia:boab_leaves"] = true
	leaf_nodenames["australia:bull_banksia_leaves"] = true
	leaf_nodenames["australia:celery_top_pine_leaves"] = true
	leaf_nodenames["australia:cherry_leaves"] = true
	leaf_nodenames["australia:cloncurry_box_leaves"] = true
	leaf_nodenames["australia:coast_banksia_leaves"] = true
	leaf_nodenames["australia:coolabah_leaves"] = true
	leaf_nodenames["australia:daintree_stringybark_leaves"] = true
	leaf_nodenames["australia:darwin_woollybutt_leaves"] = true
	leaf_nodenames["australia:desert_oak_leaves"] = true
	leaf_nodenames["australia:fan_palm_leaves"] = true
	leaf_nodenames["australia:golden_wattle_leaves"] = true
	leaf_nodenames["australia:grey_mangrove_leaves"] = true
	leaf_nodenames["australia:huon_pine_leaves"] = true
	leaf_nodenames["australia:illawarra_flame_leaves"] = true
	leaf_nodenames["australia:jarrah_leaves"] = true
	leaf_nodenames["australia:karri_leaves"] = true
	leaf_nodenames["australia:lemon_eucalyptus_leaves"] = true
	leaf_nodenames["australia:lemon_myrtle_leaves"] = true
	leaf_nodenames["australia:lilly_pilly_leaves"] = true
	leaf_nodenames["australia:macadamia_leaves"] = true
	leaf_nodenames["australia:mangrove_apple_leaves"] = true
	leaf_nodenames["australia:merbau_leaves"] = true
	leaf_nodenames["australia:marri_leaves"] = true
	leaf_nodenames["australia:moreton_bay_fig_leaves"] = true
	leaf_nodenames["australia:mulga_leaves"] = true
	leaf_nodenames["australia:paperbark_leaves"] = true
	leaf_nodenames["australia:quandong_leaves"] = true
	leaf_nodenames["australia:red_bottlebrush_leaves"] = true
	leaf_nodenames["australia:river_oak_leaves"] = true
	leaf_nodenames["australia:river_red_gum_leaves"] = true
	leaf_nodenames["australia:rottnest_island_pine_leaves"] = true
	leaf_nodenames["australia:scribbly_gum_leaves"] = true
	leaf_nodenames["australia:shoestring_acacia_leaves"] = true
	leaf_nodenames["australia:snow_gum_leaves"] = true
	leaf_nodenames["australia:southern_sassafras_leaves"] = true
	leaf_nodenames["australia:stilted_mangrove_leaves"] = true
	leaf_nodenames["australia:sugar_gum_leaves"] = true
	leaf_nodenames["australia:swamp_bloodwood_leaves"] = true
	leaf_nodenames["australia:swamp_gum_leaves"] = true
	leaf_nodenames["australia:swamp_paperbark_leaves"] = true
	leaf_nodenames["australia:tasmanian_myrtle_leaves"] = true
	leaf_nodenames["australia:tea_tree_leaves"] = true
	leaf_nodenames["australia:white_box_leaves"] = true
	leaf_nodenames["australia:wirewood_leaves"] = true
	leaf_nodenames["australia:moreton_bay_fig"] = true

end

--Support aotearoa (if present)
if minetest.get_modpath("aotearoa") then
	timber_nodenames["aotearoa:pohutukawa_tree"] = true
	timber_nodenames["aotearoa:kauri_tree"] = true
	timber_nodenames["aotearoa:karaka_tree"] = true
	timber_nodenames["aotearoa:rimu_tree"] = true
	timber_nodenames["aotearoa:totara_tree"] = true
	timber_nodenames["aotearoa:miro_tree"] = true
	timber_nodenames["aotearoa:kahikatea_tree"] = true
	timber_nodenames["aotearoa:tawa_tree"] = true
	timber_nodenames["aotearoa:black_beech_tree"] = true
	timber_nodenames["aotearoa:kamahi_tree"] = true
	timber_nodenames["aotearoa:mountain_beech_tree"] = true
	timber_nodenames["aotearoa:pahautea_tree"] = true
	timber_nodenames["aotearoa:kowhai_tree"] = true
	timber_nodenames["aotearoa:silver_beech_tree"] = true
	timber_nodenames["aotearoa:black_maire_tree"] = true
	timber_nodenames["aotearoa:hinau_tree"] = true
	timber_nodenames["aotearoa:common_tree_daisy_tree"] = true
	timber_nodenames["aotearoa:cabbage_tree_tree"] = true
	timber_nodenames["aotearoa:karo_tree"] = true
	timber_nodenames["aotearoa:kawakawa_tree"] = true
	timber_nodenames["aotearoa:mahoe_tree"] = true
	timber_nodenames["aotearoa:mamaku_tree"] = true
	timber_nodenames["aotearoa:mangrove_tree"] = true
	timber_nodenames["aotearoa:manuka_tree"] = true
	timber_nodenames["aotearoa:matagouri_tree"] = true
	timber_nodenames["aotearoa:nikau_palm_tree"] = true
	timber_nodenames["aotearoa:rangiora_tree"] = true
	timber_nodenames["aotearoa:silver_fern_tree"] = true
	timber_nodenames["aotearoa:wheki_tree"] = true

	leaf_nodenames["aotearoa:pohutukawa_leaves"] = true
	leaf_nodenames["aotearoa:kauri_leaves"] = true
	leaf_nodenames["aotearoa:karaka_leaves"] = true
	leaf_nodenames["aotearoa:karaka_fruit"] = true
	leaf_nodenames["aotearoa:rimu_leaves"] = true
	leaf_nodenames["aotearoa:totara_leaves"] = true
	leaf_nodenames["aotearoa:miro_leaves"] = true
	leaf_nodenames["aotearoa:miro_fruit"] = true
	leaf_nodenames["aotearoa:kahikatea_leaves"] = true
	leaf_nodenames["aotearoa:tawa_leaves"] = true
	leaf_nodenames["aotearoa:tawa_fruit"] = true
	leaf_nodenames["aotearoa:black_beech_leaves"] = true
	leaf_nodenames["aotearoa:kamahi_leaves"] = true
	leaf_nodenames["aotearoa:mountain_beech_leaves"] = true
	leaf_nodenames["aotearoa:pahautea_leaves"] = true
	leaf_nodenames["aotearoa:kowhai_leaves"] = true
	leaf_nodenames["aotearoa:silver_beech_leaves"] = true
	leaf_nodenames["aotearoa:black_maire_leaves"] = true
	leaf_nodenames["aotearoa:hinau_leaves"] = true
	leaf_nodenames["aotearoa:hinau_fruit"] = true
	leaf_nodenames["aotearoa:common_tree_daisy_leaves"] = true
	leaf_nodenames["aotearoa:cabbage_tree_crown"] = true
	leaf_nodenames["aotearoa:karo_leaves"] = true
	leaf_nodenames["aotearoa:kawakawa_leaves"] = true
	leaf_nodenames["aotearoa:kawakawa_fruit"] = true
	leaf_nodenames["aotearoa:mahoe_leaves"] = true
	leaf_nodenames["aotearoa:mamaku_crown"] = true
	leaf_nodenames["aotearoa:mangrove_leaves"] = true
	leaf_nodenames["aotearoa:manuka_leaves"] = true
	leaf_nodenames["aotearoa:matagouri_leaves"] = true
	leaf_nodenames["aotearoa:nikau_palm_crown"] = true
	leaf_nodenames["aotearoa:nikau_palm_skirt"] = true
	leaf_nodenames["aotearoa:rangiora_leaves"] = true
	leaf_nodenames["aotearoa:silver_fern_crown"] = true
	leaf_nodenames["aotearoa:wheki_crown"] = true
	leaf_nodenames["aotearoa:wheki_skirt"] = true
end

-- Support vines (also generated by moretrees if available)
if minetest.get_modpath("vines") then
	leaf_nodenames["vines:vines"] = true
end

if minetest.get_modpath("trunks") then
	leaf_nodenames["trunks:moss"] = true
	leaf_nodenames["trunks:moss_fungus"] = true
	leaf_nodenames["trunks:treeroot"] = true
end

-- Support jacaranda
if minetest.get_modpath("jacaranda") then
	timber_nodenames["jacaranda:trunk"] = true

	leaf_nodenames["jacaranda:blossom_leaves"] = true
end

-- Support clementinetree
if minetest.get_modpath("clementinetree") then
	timber_nodenames["clementinetree:trunk"] = true

	leaf_nodenames["clementinetree:leaves"] = true
	leaf_nodenames["clementinetree:clementine"] = true
end

-- Support lemontree
if minetest.get_modpath("lemontree") then
	timber_nodenames["lemontree:trunk"] = true

	leaf_nodenames["lemontree:leaves"] = true
	leaf_nodenames["lemontree:lemon"] = true
end

-- Support chestnuttree
if minetest.get_modpath("chestnuttree") then
	timber_nodenames["chestnuttree:trunk"] = true

	leaf_nodenames["chestnuttree:leaves"] = true
	leaf_nodenames["chestnuttree:bur"] = true
end

-- Support cherrytree
if minetest.get_modpath("cherrytree") then
	timber_nodenames["cherrytree:trunk"] = true

	leaf_nodenames["cherrytree:blossom_leaves"] = true
	leaf_nodenames["cherrytree:leaves"] = true
	leaf_nodenames["cherrytree:cherries"] = true
end

-- Support ebony
if minetest.get_modpath("ebony") then
	timber_nodenames["ebony:trunk"] = true

	leaf_nodenames["ebony:leaves"] = true
	leaf_nodenames["ebony:creeper"] = true
	leaf_nodenames["ebony:creeper_leaves"] = true
	leaf_nodenames["ebony:liana"] = true
	leaf_nodenames["ebony:persimmon"] = true
end

-- Support africatrees
if minetest.get_modpath("africatrees") then
	timber_nodenames["irokotree:trunk"] = true
	timber_nodenames["fevertree:trunk"] = true
	timber_nodenames["marula:trunk"] = true
	timber_nodenames["sycamorefig:trunk"] = true
	timber_nodenames["umbrellathorn:trunk"] = true
	timber_nodenames["africanoak:trunk"] = true
	timber_nodenames["cedarlebanon:trunk"] = true
	timber_nodenames["candelabra:trunk"] = true
	timber_nodenames["baobab:trunk"] = true

	leaf_nodenames["baobab:leaves"] = true
	leaf_nodenames["candelabra:leaves"] = true
	leaf_nodenames["cedarlebanon:leaves"] = true
	leaf_nodenames["africanoak:leaves"] = true
	leaf_nodenames["umbrellathorn:leaves"] = true
	leaf_nodenames["sycamorefig:leaves"] = true
	leaf_nodenames["marula:leaves"] = true
	leaf_nodenames["fevertree:leaves"] = true
	leaf_nodenames["irokotree:leaves"] = true
end

-- Support larch
if minetest.get_modpath("larch") then
	timber_nodenames["larch:trunk"] = true

	leaf_nodenames["larch:leaves"] = true
end

-- Support natal
if minetest.get_modpath("natal") then
	timber_nodenames["natal:prince_cycad_trunk"] = true
	timber_nodenames["natal:giant_palm_trunk"] = true
	timber_nodenames["natal:natal_cycad_trunk"] = true
	timber_nodenames["natal:woodcycad_trunk"] = true
	timber_nodenames["natal:tree_fern_trunk"] = true

	leaf_nodenames["natal:giant_palm_leaves"] = true
	leaf_nodenames["natal:natal_cycad_leaves"] = true
	leaf_nodenames["natal:prince_cycad_crown"] = true
	leaf_nodenames["natal:tree_fern_crown"] = true
	leaf_nodenames["natal:coodcycad_leaves"] = true
end


local S = technic.getter

technic.register_power_tool("technic:chainsaw_mini", chainsaw_max_charge_mini)
technic.register_power_tool("technic:chainsaw", chainsaw_max_charge)
technic.register_power_tool("technic:chainsaw_mk2", chainsaw_max_charge_mk2)

-- Table for saving what was sawed down
local produced = {}

-- Save the items sawed down so that we can drop them in a nice single stack
local function handle_drops(drops)
	for _, item in ipairs(drops) do
		local stack = ItemStack(item)
		local name = stack:get_name()
		local p = produced[name]
		if not p then
			produced[name] = stack
		else
			p:set_count(p:get_count() + stack:get_count())
		end
	end
end

-- Removes a single leaf node, unless there's reason not to
local function mow_leaf(pos, current_charge, node)
	-- Do not remove leaf if there's an adjacent log
	if timber_nodenames[minetest.get_node({x = pos.x, y = pos.y, z = pos.z + 1}).name]
		or timber_nodenames[minetest.get_node({x = pos.x, y = pos.y, z = pos.z - 1}).name]
		or timber_nodenames[minetest.get_node({x = pos.x, y = pos.y + 1, z = pos.z}).name]
		or timber_nodenames[minetest.get_node({x = pos.x, y = pos.y - 1, z = pos.z}).name]
		or timber_nodenames[minetest.get_node({x = pos.x + 1, y = pos.y, z = pos.z}).name]
		or timber_nodenames[minetest.get_node({x = pos.x - 1, y = pos.y, z = pos.z}).name] then
		return current_charge
	end

		-- Leaves found to be removed
	handle_drops(minetest.get_node_drops(node.name, ""))
	minetest.remove_node(pos)
	return current_charge - chainsaw_charge_per_leaf

end

-- Removes nearby leaves
local function mow_leaves(pos, current_charge, username)
	local remaining_charge = current_charge
	for y = pos.y, pos.y + leaves_reach do
		for x = pos.x - leaves_reach, pos.x + leaves_reach do
			for z = pos.z - leaves_reach, pos.z + leaves_reach do
				if remaining_charge < chainsaw_charge_per_leaf then
					return remaining_charge
				end
				local mpos = {x = x, y = y, z = z}
				local node = minetest.get_node(mpos)
				if leaf_nodenames[node.name]
					and not minetest.is_protected(mpos, username) then
					remaining_charge = mow_leaf(mpos, remaining_charge, node)
				end
			end
		end
	end
	return remaining_charge
end


-- This function does all the hard work. Recursively we dig the node at hand
-- if it is in the table and then search the surroundings for more stuff to dig.
local function recursive_dig(pos, remaining_charge, username, side_limit, lower_bound)
	if remaining_charge < chainsaw_charge_per_log then
		return remaining_charge
	end
	local node = minetest.get_node(pos)

	if not timber_nodenames[node.name] then
		if not leaf_nodenames[node.name] then
			return remaining_charge
		end
	end

	if minetest.is_protected(pos, username) then
		return remaining_charge
	end

	-- Wood found - cut it
	handle_drops(minetest.get_node_drops(node.name, ""))
	minetest.remove_node(pos)
	remaining_charge = remaining_charge - chainsaw_charge_per_log

	-- Check surroundings and run recursively if any charge left
	local function try_dig(pos, side_limit)
		if remaining_charge < chainsaw_charge_per_log then
			return
		end
		if timber_nodenames[minetest.get_node(pos).name] then
			remaining_charge = recursive_dig(pos, remaining_charge, username, side_limit, lower_bound)
		elseif leaf_nodenames[minetest.get_node(pos).name] then
			remaining_charge = mow_leaves(pos, remaining_charge, username)
		end
	end

	-- first we try to ascend
	-- corners
	try_dig({x = pos.x + 1, y = pos.y + 1, z = pos.z - 1}, side_limit, lower_bound)
	try_dig({x = pos.x - 1, y = pos.y + 1, z = pos.z + 1}, side_limit, lower_bound)
	try_dig({x = pos.x - 1, y = pos.y + 1, z = pos.z - 1}, side_limit, lower_bound)
	try_dig({x = pos.x + 1, y = pos.y + 1, z = pos.z + 1}, side_limit, lower_bound)

	-- edges
	try_dig({x = pos.x + 1, y = pos.y + 1, z = pos.z    }, side_limit, lower_bound)
	try_dig({x = pos.x    , y = pos.y + 1, z = pos.z - 1}, side_limit, lower_bound)
	try_dig({x = pos.x    , y = pos.y + 1, z = pos.z + 1}, side_limit, lower_bound)
	try_dig({x = pos.x - 1, y = pos.y + 1, z = pos.z    }, side_limit, lower_bound)

	-- straight up
	try_dig({x = pos.x    , y = pos.y + 1, z = pos.z    }, side_limit, lower_bound)

	-- then attempt the sides
	if (side_limit >= 1) then
		-- edges
		try_dig({x = pos.x + 1, y = pos.y    , z = pos.z    }, side_limit - 1, lower_bound)
		try_dig({x = pos.x    , y = pos.y    , z = pos.z - 1}, side_limit - 1, lower_bound)
		try_dig({x = pos.x    , y = pos.y    , z = pos.z + 1}, side_limit - 1, lower_bound)
		try_dig({x = pos.x - 1, y = pos.y    , z = pos.z    }, side_limit - 1, lower_bound)
		-- corners
		try_dig({x = pos.x + 1, y = pos.y    , z = pos.z - 1}, side_limit - 1, lower_bound)
		try_dig({x = pos.x - 1, y = pos.y    , z = pos.z + 1}, side_limit - 1, lower_bound)
		try_dig({x = pos.x - 1, y = pos.y    , z = pos.z - 1}, side_limit - 1, lower_bound)
		try_dig({x = pos.x + 1, y = pos.y    , z = pos.z + 1}, side_limit - 1, lower_bound)

	end

	if (pos.y > lower_bound and side_limit >= 1) then
		-- edges
		try_dig({x = pos.x + 1, y = pos.y - 1, z = pos.z    }, side_limit - 1, lower_bound)
		try_dig({x = pos.x    , y = pos.y - 1, z = pos.z - 1}, side_limit - 1, lower_bound)
		try_dig({x = pos.x    , y = pos.y - 1, z = pos.z + 1}, side_limit - 1, lower_bound)
		try_dig({x = pos.x - 1, y = pos.y - 1, z = pos.z    }, side_limit - 1, lower_bound)
		-- corners
		try_dig({x = pos.x + 1, y = pos.y - 1, z = pos.z - 1}, side_limit - 1, lower_bound)
		try_dig({x = pos.x - 1, y = pos.y - 1, z = pos.z + 1}, side_limit - 1, lower_bound)
		try_dig({x = pos.x - 1, y = pos.y - 1, z = pos.z - 1}, side_limit - 1, lower_bound)
		try_dig({x = pos.x + 1, y = pos.y - 1, z = pos.z + 1}, side_limit - 1, lower_bound)

	end

	return remaining_charge
end


-- Function to randomize positions for new node drops
local function get_drop_pos(pos)
	local drop_pos = {}

	for i = 0, 8 do
		-- Randomize position for a new drop
		drop_pos.x = pos.x + math.random(-3, 3)
		drop_pos.y = pos.y - 1
		drop_pos.z = pos.z + math.random(-3, 3)

		-- Move the randomized position upwards until
		-- the node is air or unloaded.
		for y = drop_pos.y, drop_pos.y + 5 do
			drop_pos.y = y
			local node = minetest.get_node_or_nil(drop_pos)

			if not node then
				-- If the node is not loaded yet simply drop
				-- the item at the original digging position.
				return pos
			elseif node.name == "air" then
				-- Add variation to the entity drop position,
				-- but don't let drops get too close to the edge
				drop_pos.x = drop_pos.x + (math.random() * 0.8) - 0.5
				drop_pos.z = drop_pos.z + (math.random() * 0.8) - 0.5
				return drop_pos
			end
		end
	end

	-- Return the original position if this takes too long
	return pos
end


-- Chainsaw entry point
local function chainsaw_dig(pos, current_charge, username)
	-- Start sawing things down
	-- The loops make the starting position effectively a 3x1x3 area in order to
	-- not become too restrictive for practical usage
	local remaining_charge = current_charge
	local dpos_table = {0, -1, 1}
	local original_pos = {x = pos.x, y = pos.y, z = pos.z}
	local dpos = {x = 0, y = 0, z = 0}
	for x = 1, 3 do
		for z = 1, 3 do
			dpos.x = original_pos.x + dpos_table[x]
			dpos.y = original_pos.y
			dpos.z = original_pos.z + dpos_table[z]
			remaining_charge = recursive_dig(dpos, remaining_charge, username, 30, pos.y)
			if remaining_charge < chainsaw_charge_per_log then break end;
		end
		if remaining_charge < chainsaw_charge_per_log then break end;
	end

	minetest.sound_play("chainsaw", {pos = pos, gain = 1.0,
			max_hear_distance = 10})

	-- Now drop items for the player
	for name, stack in pairs(produced) do
		-- Drop stacks of stack max or less
		local count, max = stack:get_count(), stack:get_stack_max()
		stack:set_count(max)
		while count > max do
			minetest.add_item(get_drop_pos(pos), stack)
			count = count - max
		end
		stack:set_count(count)
		minetest.add_item(get_drop_pos(pos), stack)
	end

	-- Clean up
	produced = {}

	return remaining_charge
end


local function use_chainsaw(itemstack, user, pointed_thing, mk)
	if pointed_thing.type ~= "node" then
		return itemstack
	end

	local meta = minetest.deserialize(itemstack:get_metadata())
	if not meta or not meta.charge or
			meta.charge < chainsaw_charge_per_log then
		return
	end

	local name = user:get_player_name()
	if minetest.is_protected(pointed_thing.under, name) then
		minetest.record_protection_violation(pointed_thing.under, name)
		return
	end

	-- Send current charge to digging function so that the
	-- chainsaw will stop after digging a number of nodes
	meta.charge = chainsaw_dig(pointed_thing.under, meta.charge, name)
	if not technic.creative_mode then
		if mk == 1 then
			technic.set_RE_wear(itemstack, meta.charge, chainsaw_max_charge)
		elseif mk == 2 then
			technic.set_RE_wear(itemstack, meta.charge, chainsaw_max_charge_mk2)
		else
			technic.set_RE_wear(itemstack, meta.charge, chainsaw_max_charge_mini)
		end
		itemstack:set_metadata(minetest.serialize(meta))
	end
	return itemstack
end

minetest.register_tool("technic:chainsaw", {
	description = S("Chainsaw"),
	inventory_image = "technic_chainsaw.png",
	stack_max = 1,
	groups = {technic_tool = 1, technic_powertool = 1},
	wear_represents = "technic_RE_charge",
	on_refill = technic.refill_RE_charge,
	on_use = function(itemstack, user, pointed_thing)
			use_chainsaw(itemstack, user, pointed_thing, 1)
			return(itemstack)
		end
})

minetest.register_tool("technic:chainsaw_mk2", {
	description = S("Chainsaw Mk2"),
	inventory_image = "technic_chainsaw_mk2.png",
	stack_max = 1,
	groups = {technic_tool = 1, technic_powertool = 1},
	wear_represents = "technic_RE_charge",
	on_refill = technic.refill_RE_charge,
	on_use = function(itemstack, user, pointed_thing)
			use_chainsaw(itemstack, user, pointed_thing, 2)
			return(itemstack)
		end
})

minetest.register_tool("technic:chainsaw_mini", {
	description = S("Chainsaw Mini"),
	inventory_image = "technic_chainsaw_mini.png",
	stack_max = 1,
	groups = {technic_tool = 1, technic_powertool = 1},
	wear_represents = "technic_RE_charge",
	on_refill = technic.refill_RE_charge,
-- 	groups = {not_in_creative_inventory = 1},
	on_use = function(itemstack, user, pointed_thing)
			use_chainsaw(itemstack, user, pointed_thing, nil)
			return(itemstack)
		end
})

local mesecons_button = minetest.get_modpath("mesecons_button")
local trigger = mesecons_button and "mesecons_button:button_off" or "default:mese_crystal_fragment"

minetest.register_craft({
	output = "technic:chainsaw_mini",
	recipe = {
		{"technic:wrought_iron_ingot", "technic:wrought_iron_ingot", "technic:battery"},
		{"",                           "technic:motor",              trigger},
		{"",                           "",                           ""},
	}
})

minetest.register_craft({
	output = "technic:chainsaw",
	recipe = {
		{"technic:stainless_steel_ingot", trigger,         "technic:battery"},
		{"technic:fine_copper_wire",      "technic:motor", "technic:battery"},
		{"",                              "",              "technic:stainless_steel_ingot"},
	}
})

minetest.register_craft({
	output = "technic:chainsaw_mk2",
	recipe = {
		{"technic:chainsaw",             "technic:stainless_steel_ingot", "technic:stainless_steel_ingot"},
		{"technic:battery",              "technic:battery",               ""},
		{"technic:battery",              "dye:green",                     ""},
	}
})

